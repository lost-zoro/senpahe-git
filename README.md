# senpahe

download or stream anime from the sites animepahe.com, animepahe.ru and animepahe.org

need to have openssl, ffmpeg and aria2 for the downloader to work.
need to have mpv for the streamer to work.

To use the downloader, pass a search term after the argument -s
e.g. senpahe -s naruto

Or, pass a link after the argument -d
e.g. senpahe -d example.com

To use the streamer just enter senpahe

Only for linux at the moment
