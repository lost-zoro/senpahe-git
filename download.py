import pprint
import time
import os
import requests
import codecs
import threading
import sys
from os import walk
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from pathlib import Path

home = str(Path.home())

DEFAULT_TIMEOUT = 5  # seconds
progress = 0


class TimeoutHTTPAdapter(HTTPAdapter):
    def __init__(self, *args, **kwargs):
        self.timeout = DEFAULT_TIMEOUT
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(*args, **kwargs)

    def send(self, request, **kwargs):
        timeout = kwargs.get("timeout")
        if timeout is None:
            kwargs["timeout"] = self.timeout
        return super().send(request, **kwargs)


def search(val):
    if val == 'bad':
        keyw = input("Enter search term: ")
    else:
        keyw = val
    succ = 0
    while(succ == 0):
        try:
            rr = requests.get(
                "https://animepahe-com.translate.goog/api?m=search&l=8&q=" + keyw)
        except:
            pass
        else:
            succ += 1

    jr = rr.json()
    invalid = 0
    try:
        sa = jr['data']
    except:
        print("Invalid term try again...")
        val = 'bad'
        sa = search()
        invalid = 1
    if invalid != 1:
        for i, j in zip(sa, range(1, int(jr['total']) + 1)):
            print(j, end=') ')
            print("Title: ", i['title'])
            print("Type: ", i['type'])
            print("Number of episodes: ", i['episodes'])
            print("Status: ", i['status'])
            print("Aired: ", i['season'], end=' ')
            print(i['year'], end='\n\n')
        if input("See the required anime in the results?(y/n) ") == 'n':
            val = 'bad'
            sa = search(val)
    return sa


#def drawProgressBar(percent, barLen=20):
#    sys.stdout.write("\r")
#    sys.stdout.write("[{:<{}}] {:.0f}%".format(
#        "=" * int(barLen * percent), barLen, percent * 100))
#    sys.stdout.flush()


#def download(a, ref, siz):
#    retries = Retry(total=10, backoff_factor=1,
#                    status_forcelist=[429, 500, 502, 503, 504])
#    http = requests.Session()
#    http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
#    print("starting " + a[a.find("segment-"):])
#    global progress
#   if progress > 0:
#       progress = 0
#
#    try:
#        r = http.get(a, headers={'referer': ref}, timeout=20)
#        print("requested " + a[a.find("segment-"):])
#        print("response code is ", a[a.find("segment-"):], " ", r)
#        f = open(a[a.find("segment-"):], 'wb')
#        f.write(r.content)
#    except:
#        try:
#            f.close()
#        except:
#            pass
#        download(a, ref, siz)
#    try:
#        f.close()
#    except:
#        pass
##    os.system('wget "--header=Referer: ' + ref + '" "' + a + '"')
#    print("downloaded " + a[a.find("segment-"):])
#    #print("Size of file is :", f.tell(), "bytes")
#    #mm = int(os.popen('wc -c < ' + a[a.find("segment-"):]).read())
#    mm = int(r.headers['Content-length'])
#    progress += mm
#    print("total \% done: ", (progress/int(siz)) * 100)
#    #drawProgressBar((progress/int(siz)), 100)

def svrscrape(ll, b):
    svrlist = []
    svrlist.append('https://eu-a1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-b1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-c1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-d1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')

    svrlist.append('https://eu-001.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')

    for i in range(11, 1000, 10):
        if i < 100:
            svrlist.append('https://eu-' + '0' + str(i) + '.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
        else:
            svrlist.append('https://eu-' + str(i) + '.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    #print(svrlist)
    t = []
    def see(uwu, b):
        try:
            with open('done.aa') as f:
                print(f.readlines())
        except IOError:
            pass
        else:
            return
        try:
            tmp = os.popen('curl -s --connect-timeout 5 --max-time 5 --retry 5 --retry-delay 5 --retry-max-time 5 "' + uwu + '" -H "Referer: ' + b + '"').read()
        except:
            print("exception ", tmp)
        try:
            if tmp[0] != '#':
                print("m3u8 not found")
                return
            elif tmp[0] == '#':
                with open('uwu.m3u8', 'w') as ff:
                    ff.write(tmp)
                with open('done.aa', 'w') as ff:
                    ff.write(uwu)
        except:
            sys.exit(0)
    for i in svrlist:
        t.append(threading.Thread(target=see, args=(i,b,)))
    toret = ''
    for i in range(0, 104):
        try:
            with open('done.aa') as f:
                toret = f.readline()
        except IOError:
            t[i].start()
            #sleep(0.2)
            time.sleep(0.4)
        else:
            break
    #for i in t:
        #i.join()
    return toret

# Path where you want to save/load cookies to/from aka C:\my\fav\directory\cookies.txt


def main(meth, val):

    print("Download mode!")
    if meth == 1:
        sa = search(val)
        selection = 1
        selection = int(input("Which search result is required? "))
        anime_id = str(sa[selection-1]['id'])
        aniname = str(sa[selection-1]['title'])
        anime_session = str(sa[selection-1]['session'])
        link = "https://animepahe-com.translate.goog/anime/" + \
            sa[selection-1]['session'] + ""
        # print(link)
        succ = 0
        while(succ == 0):
            try:
                r = requests.get(link)
            except:
                pass
            else:
                succ += 1

    else:
        link = val

        r = requests.get('https://animepahe-ru.translate.goog' + link[20:] + '?_x_tr_sl=fr&_x_tr_tl=en')
        #print('https://animepahe-ru.translate.goog' + link[20:] + '?_x_tr_sl=fr&_x_tr_tl=en')
        #print('https://dlmi2r3ia44utzkrktwq2ouhty--animepahe-org.translate.goog' + link[20:])
        num1 = r.text.find('<div class="tab-content anime-detail')
        num2 = r.text.find('row', num1)
        anime_session = link[27:]
        anime_id = r.text[num1+43:num2-1]
        #print(anime_id)
        aniname = r.text[r.text.find('<h1>') + 4: r.text.find('</h1>')]
    # print(anime_id)
    # cookie change
##    options = Options()
##    options.headless = True
##    driver = selenium.webdriver.Firefox(options=options)
# driver.get(link)
##    save_cookies(driver, "cfduid")
# driver.quit()
##    cookies = pickle.load(open("cfduid", "rb"))
# for i in cookies:
# if i['name'] == '__cfduid':
##            cfduid = i['value']
    # end
    num1 = r.text.find('<h1>')
    num2 = r.text.find('<', num1 + 1)
    #aniname = r.text[num1+4:num2]
    print("Name: ", aniname)
    if ':' in aniname:
        aniname = aniname.replace(':', ' -')
    succ = 0
    print(anime_id)

    while(succ == 0):
        try:
            r = requests.get("https://animepahe-com.translate.goog/api?m=release&id=" +
                             anime_id+"&l=30&sort=episode_asc&page=1")
        except:
            pass
        else:
            succ += 1

    jsonResponse = r.json()
    epno = jsonResponse['total']
    print("Total episodes: ", epno)

    start = int(input("Enter episode to start at: "))
    end = int(input("Enter episode to end at: "))

    res = ''
##    cdn = input("\nWanna use CDN?(y/n)(not available on saturdays and sundays, check during the first stage to be sure) ")
    cdn = 'n'
    x = []
    try:
        with open(home + '/.config/pahe_location.conf') as f:
            sav = f.readline()
            if sav[-1] == '\n' or sav[-1] == '/':
                sav = sav[:-1]
            print("Got the path: " + sav + "\nFrom pahe_location.conf!")
            time.sleep(0.5)
    except IOError:
        sav = input("Where to save? (Enter absolute path or leave blank if you wanna download in this folder)")
        print("(create a file pahe_location.conf in your ~/.config folder containing the absolute path to where you wanna download the anime)")
        time.sleep(0.5)
    direct = dict()
    lcc = ''
    for i in range(start, end + 1):
        multi = int(i/30)
        if i % 30 == 0:
            multi -= 1
        index = int(i - 1 - (multi*30))
        succ = 0
        while(succ == 0):
            try:
                r = requests.get("https://animepahe-com.translate.goog/api?m=release&id=" +
                                 anime_id+"&l=30&sort=episode_asc&page=" + str(multi + 1))
            except:
                pass
            else:
                succ += 1

        jsonResponse = r.json()
        session = jsonResponse['data'][index]['session']
        # print(session)
        succ = 0
        while(succ == 0):
            try:
                rr = requests.get(
                    "https://animepahe-com.translate.goog/api?m=links&id="+anime_id+"&session="+session)
            except:
                pass
            else:
                succ += 1

        jsonResponse = rr.json()
        reslist = jsonResponse['data']

        if i == start:
            print("\nAvailable resolutions for episode(s) are:" + '\n')
            if start - end == 0:

                for j in reslist:
                    tmp = ''
                    tmp1 = float(j[str(list(j))[2:-2]]['filesize'])
                    byt = ''
                    if tmp1 > 1073741824:
                        byt = str(
                            float("{:.2f}".format(tmp1/1073741824))) + ' GB'
                    else:
                        byt = str(float("{:.2f}".format(tmp1/1048576))) + ' MB'
                    if j[str(list(j))[2:-2]]['hq'] == 1:
                        tmp = str(list(j))[2:-2] + 'p' + ' Fansub Provider: ' + str(
                            j[str(list(j))[2:-2]]['fansub']) + ' Size: ' + byt + ' HQ(Larger size)'
                    else:
                        tmp = str(list(j))[2:-2] + 'p' + ' Fansub Provider: ' + \
                            str(j[str(list(j))[2:-2]]['fansub']) + \
                            ' Size: ' + byt

                    x.append(str(list(j)))
                    print(tmp)
            else:

                for j in reslist:
                    tmp = ''
                    if j[str(list(j))[2:-2]]['hq'] == 1:
                        tmp = str(list(j))[
                            2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])
                    else:
                        tmp = str(list(j))[
                            2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])

                    x.append(str(list(j)))
                    print(tmp)
            res = int(input(
                "\nWhich resolution do you want to download in?(1 for first one, 2 for second one) "))
            if sav != '':
                savfinal = sav + '/' + aniname
                #print(savfinal)
            else:
                savfinal = aniname

            try:
                os.makedirs(savfinal)
            except:
                print("\nFolder already exists!! Continuing download inside it...")

        try:
            b = jsonResponse['data'][res - 1][x[res-1][2:-2]]['kwik']
        except:
            os.system(
                'notify-send "senpahe" "You need to choose the resolution again"')
            print("\nAvailable resolutions for episode(s) are:" + '\n')
            y = []
            for j in reslist:
                tmp = ''
                if j[str(list(j))[2:-2]]['hq'] == 1:
                    tmp = str(list(j))[
                        2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])
                else:
                    tmp = str(list(j))[
                        2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])

                y.append(str(list(j)))
                print(tmp)
            res = int(input(
                "\nWhich resolution do you want to download in?(1 for first one, 2 for second one) "))
            b = jsonResponse['data'][res - 1][y[res-1][2:-2]]['kwik']
            siz = jsonResponse['data'][res - 1][y[res-1][2:-2]]['filesize']
        else:
            siz = jsonResponse['data'][res - 1][x[res-1][2:-2]]['filesize']

        referer = "https://animepahe.com/play/" + anime_session + "/" + session
        # get link

# def dlpage(kw, ref):
##            retries = Retry(total=100, backoff_factor=1, status_forcelist=[429, 500, 502, 503, 504])
##            http = requests.Session()
##            http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
# try:
##                rf = http.get(kw, headers={'referer':ref}).content
# except:
##                rf = download(kw, ref)
# return rf
##        rr = dlpage(b, referer)
# print(rr)
        getnamelink = 'curl "' + b + '" -H "Referer: ' + referer + '"'

        print(getnamelink)
        rr = os.popen(getnamelink).read()
        epname = rr[rr.find("Anime"):rr.find(".mp4") + 4]

        try:
            os.chdir(savfinal)
        except:
            pass
        rt = rr.find("/snapshot/")
        uwu = ''
        ll = rr[rt+10:rr.find('.', rt+10)]
        tmp = ''
        if lcc == '':
            uwu = svrscrape(ll, b)
            os.remove('done.aa')
            with open("uwu.m3u8", 'r') as f:
                s = ''
                while s[0:5] != 'https':
                    s = f.readline()
                lcc = s[11:s.find('.', 11)]
        else:
            def dluwu(uwu, ref):
                retries = Retry(total=100, backoff_factor=1,
                                status_forcelist=[429, 500, 502, 503, 504])
                http = requests.Session()
                http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
                f = open('uwu.m3u8', 'wb')
                try:
                    r = http.get(uwu, headers={'referer': ref}, timeout=5)
                    print("status_code is ", r.status_code)
                    if r.status_code == 200:
                        f.write(r.content)
                    else:
                        return -1
                except:
                    f.close()
                    #download(uwu, ref)
                try:
                    f.close()
                except:
                    pass
                return 0
            uwu = "https://eu-"+lcc+".files.nextcdn.org/stream/" + ll + "/uwu.m3u8"
            if dluwu(uwu, b) == -1:
                print("!!!!!!!SERVER CHANGED!!!!!!!!")
                uwu = svrscrape(ll, b)
                os.remove('done.aa')
                with open("uwu.m3u8", 'r') as f:
                    s = ''
                    while s[0:5] != 'https':
                        s = f.readline()
                    lcc = s[11:s.find('.', 11)]

        print(b)
        print(uwu)

        print(ll)

        print(epname)
        progress = 0

        def dlmon(uwu, ref):
            retries = Retry(total=100, backoff_factor=1,
                            status_forcelist=[429, 500, 502, 503, 504])
            http = requests.Session()
            http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
            f = open('mon.key', 'wb')
            try:
                f.write(http.get(uwu[:-8] + 'mon.key',
                        headers={'referer': ref}, timeout=5).content)
            except:
                f.close()
                #download(uwu[:-8], ref)
            try:
                f.close()
            except:
                pass
        dlmon(uwu, b)
        keyhex = []
        with open('mon.key', 'rb') as f:
            for chunk in iter(lambda: f.read(32), b''):
                keyhex.append(codecs.encode(chunk, 'hex'))
        key = str(keyhex[0])[2:-1]
        print(key)
        ass = []
        with open("uwu.m3u8", 'r') as f:
            for i in range(0, 8):
                s = f.readline()
            while (s != ''):

                s = f.readline()[:-1]
                #rint(s, end='\n')
                ass.append(s)
                s = f.readline()
        ass.pop()
        os.system('aria2c -i uwu.m3u8 --referer="' + b + '" --disable-ipv6')
#        for i in ass:
#            t.append(threading.Thread(target=download, args=(i, b, siz,)))

#        segno = len(t)
#        limit = 300

#        if segno < limit:
#            for i in t:
#                i.start()
# time.sleep(0.4)
#            for i in t:
#                i.join()
#        else:
#            mplier = int(segno/limit)
#            for i in range(0, segno * mplier, limit):
#                print("VALUE OF I IS ", i)
#                for j in t[i:i + limit]:
#                    j.start()
# time.sleep(0.4)
#                for j in t[i:i + limit]:
#                    j.join()
#            for i in t[(segno * mplier) + 1:]:
#                i.start()
# time.sleep(0.4)
#            for i in t[(segno * mplier) + 1:]:
#                i.join()

        f = open("uwu.m3u8", 'r')
        segments = ""
        foo = []
        tempvar = f.read()
        segs, sege = 0, 0
        f.close()
        while(1):
            segs = tempvar.find("segment", sege)
            sege = tempvar.find(".ts", segs)
            if tempvar[segs:sege] == '':
                break
            foo.append(tempvar[segs:sege]+tempvar[sege:sege+3])
        with open("output.ts", 'wb') as ots:
            for i in foo:
                with open(i, 'rb') as seggs:
                    ots.write(seggs.read())

        os.system("openssl aes-128-cbc -md sha512 -d -K " + key +
                  " -iv " + key + " -salt -in output.ts -out outputaa.ts")

        os.system(
            'ffmpeg -hide_banner -loglevel error -i outputaa.ts -map 0 -c copy ' + epname)

        os.remove("outputaa.ts")
        os.remove("uwu.m3u8")
        os.remove("mon.key")
        for i in foo:
            os.remove(i)
        os.remove("output.ts")
        #os.system('move ' + epname + ' "..\\' + aniname + '"')
        if i == end:
            print("Finished!")
            #sys.exit(0)
        else:
            continue
        # end
        #hmm = jsonResponse['data'][res - 1][x[res-1][2:-2]]['filesize']
        # if hmm > 1073741824:
        #    sizz = str(float("{:.2f}".format(hmm/1073741824))) + ' GB'
        # else:
        #    sizz = str(float("{:.2f}".format(hmm/1048576))) + ' MB'

        #flink = ll.replace("/e/", "/f/")
        #dlink = ll.replace("/f/", "/d/")

        #print("Getting links for episode: ", i, " (" + sizz + ")")
        # if cdn == 'y':
        #    getlink = 'curl -s "'+dlink+'" -H "Origin: https://kwik.cx" -H "Referer: '+flink+'" -H "Cookie: __cfduid='+cfduid+'; cf_clearance='+cfclearence+'; kwik_session='+kwiksession+'; a='+a+'; '+sb1[0] + "=" + sb1[1]+'; '+sb2[0] + "=" + sb2[1]+'; '+sb3[0] + "=" + sb3[1]+'; '+sb4[0] + "=" + sb4[1]+';" --data-raw "_token='+token+'&_cf=1"'
        # else:
            #getlink = 'curl -s "'+dlink+'" -H "Origin: https://kwik.cx" -H "Referer: '+flink+'" -H "Cookie: __cfduid='+cfduid+'; cf_clearance='+cfclearence+'; kwik_session='+kwiksession+'; a='+a+'; '+sb1[0] + "=" + sb1[1]+'; '+sb2[0] + "=" + sb2[1]+'; '+sb3[0] + "=" + sb3[1]+'; '+sb4[0] + "=" + sb4[1]+';" --data-raw "_token='+token+'"'
            #getlink = 'curl -s "'+dlink+'" -H "Origin: https://kwik.cx" -H "Referer: '+flink+'" -H "Cookie: __cfduid='+cfduid+'; cf_clearance='+cfclearence+'; kwik_session='+kwiksession+'; " --data-raw "_token='+token+'"'

        #d = os.popen(getlink).read()
        # print(getlink)
        #lstart = d.find('"', d.find('href='))
        #lend = d.find('"', lstart+1)
        #downlink = d[lstart+1:lend]

        #amp = downlink.find('amp;')
        #downlink = downlink[:amp] + downlink[amp+4:]

        #amp = downlink.find('amp;')
        #downlink = downlink[:amp] + downlink[amp+4:]

        #eplink = 'wget -c "' + downlink + '" --header "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0" --header "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" --header "Accept-Language: en-US,en;q=0.5" --header "Referer: '+flink+'" --header "DNT: 1" --header "Connection: keep-alive" --header "Upgrade-Insecure-Requests: 1" --content-disposition -q --show-progress'
        #eplink = 'curl -O -J -L "'+downlink+'" -H "Referer: '+flink+'" -H "Cookie: __cfduid='+cfduid+'"'
        #direct[str(i) + ' (' + sizz + ')'] = eplink

    #download(direct, savfinal)
    print('\n' + "Downloads Finished!")
    #sys.exit(0)
#try:
#    main()
#except KeyboardInterrupt:
#    print("Interrupted by user")
