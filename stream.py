import pprint
import time
import json
import os
import requests
import codecs
import threading
import sys
from os import walk
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from subprocess import run

DEFAULT_TIMEOUT = 5  # seconds
progress = 0


class TimeoutHTTPAdapter(HTTPAdapter):
    def __init__(self, *args, **kwargs):
        self.timeout = DEFAULT_TIMEOUT
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(*args, **kwargs)

    def send(self, request, **kwargs):
        timeout = kwargs.get("timeout")
        if timeout is None:
            kwargs["timeout"] = self.timeout
        return super().send(request, **kwargs)


def search():
    keyw = input("Enter search term: ")
    succ = 0
    while(succ == 0):
        try:
            rr = requests.get(
                "https://animepahe-com.translate.goog/api?m=search&l=8&q=" + keyw)
        except:
            pass
        else:
            succ += 1

    jr = rr.json()
    invalid = 0
    try:
        sa = jr['data']
    except:
        print("Invalid term try again...")
        sa = search()
        invalid = 1
    if invalid != 1:
        for i, j in zip(sa, range(1, int(jr['total']) + 1)):
            print(j, end=') ')
            print("Title: ", i['title'])
            print("Type: ", i['type'])
            print("Number of episodes: ", i['episodes'])
            print("Status: ", i['status'])
            print("Aired: ", i['season'], end=' ')
            print(i['year'], end='\n\n')
        if input("See the required anime in the results?(y/n) ") == 'n':
            sa = search()
    return sa

# Path where you want to save/load cookies to/from aka C:\my\fav\directory\cookies.txt
def mpvplay(uwu,b):
    print("mpv --input-ipc-server=/tmp/mpvsocket --referrer="+b+" "+uwu)
    stream = os.popen("mpv --input-ipc-server=/tmp/mpvsocket --referrer="+b+" "+uwu)

    out = (stream.read())
    print(out)
    if "End of file" not in out:
        print("Closed by the user.")
        os.remove('uwu.m3u8')
        sys.exit(0)

def lastep(aniname,res,start):
    stream = os.popen("socat - /tmp/mpvsocket")
    a = stream.read()
    a = a.split("\n")
    current = int(a[-2][a[-2].find('playlist_entry_id')+19:a[-2].find('}')])
    d = {aniname: {'ep':current+start-1,'res':res}}
    try:
        f = open(os.path.expanduser('~') + '/.config/senpahe_lastep.json', 'rb')
        temp = json.load(f)
        temp.update(d)
        f.close()
    except:
        pass
    else:
        with open(os.path.expanduser('~') + '/.config/senpahe_lastep.json', 'w', encoding='utf-8') as f:
            json.dump(temp, f, ensure_ascii=False, indent=4)
#   os.remove(".temp_playlist")
def svrscrape(ll, b):
    svrlist = []
    svrlist.append('https://eu-a1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-b1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-c1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    svrlist.append('https://eu-d1.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')

    svrlist.append('https://eu-001.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')

    for i in range(11, 1000, 10):
        if i < 100:
            svrlist.append('https://eu-' + '0' + str(i) + '.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
        else:
            svrlist.append('https://eu-' + str(i) + '.files.nextcdn.org/stream/' + ll + '/uwu.m3u8')
    #print(svrlist)
    t = []
    def see(uwu, b):
        try:
            with open('done.aa') as f:
                print(f.readlines())
        except IOError:
            pass
        else:
            return
        try:
            tmp = os.popen('curl -s --connect-timeout 5 --max-time 5 --retry 5 --retry-delay 5 --retry-max-time 5 "' + uwu + '" -H "Referer: ' + b + '"').read()
        except:
            print("exception ", tmp)
        try:
            if tmp[0] != '#':
                print("m3u8 not found")
                return
            elif tmp[0] == '#':
                with open('uwu.m3u8', 'w') as ff:
                    ff.write(tmp)
                with open('done.aa', 'w') as ff:
                    ff.write(uwu)
        except:
            exit(0)
    for i in svrlist:
        t.append(threading.Thread(target=see, args=(i,b,)))
    toret = ''
    for i in range(0, 104):
        try:
            with open('done.aa') as f:
                toret = f.readline()
        except IOError:
            t[i].start()
            #sleep(0.2)
            time.sleep(0.4)
        else:
            break
    #for i in t:
        #i.join()
    return toret



def main():

    print("Streaming mode! Tip use -d flag to enter download mode")
    meth = int(input(
        "\nSelect type of operation:\n1) Search for anime\n2) Paste link from browser\n"))
    if meth == 1:
        sa = search()
        selection = 1
        selection = int(input("Which search result is required? "))
        anime_id = str(sa[selection-1]['id'])
        aniname = str(sa[selection-1]['title'])
        anime_session = str(sa[selection-1]['session'])
        link = "https://animepahe-com.translate.goog/anime/" + \
            sa[selection-1]['session'] + ""
        # print(link)
        succ = 0
        while(succ == 0):
            try:
                r = requests.get(link)
            except:
                pass
            else:
                succ += 1

    else:
        link = input("Enter link of anime from animepahe: ")
        r = requests.get('https://animepahe-ru.translate.goog' + link[20:] + '?_x_tr_sl=fr&_x_tr_tl=en')
        #print('https://animepahe-ru.translate.goog' + link[20:] + '?_x_tr_sl=fr&_x_tr_tl=en')
        #print('https://dlmi2r3ia44utzkrktwq2ouhty--animepahe-org.translate.goog' + link[20:])
        num1 = r.text.find('<div class="tab-content anime-detail')
        num2 = r.text.find('row', num1)
        anime_session = link[27:]
        anime_id = r.text[num1+43:num2-1]
        #print(anime_id)
        aniname = r.text[r.text.find('<h1>') + 4: r.text.find('</h1>')]
    # print(anime_id)
    # cookie change
##    options = Options()
##    options.headless = True
##    driver = selenium.webdriver.Firefox(options=options)
# driver.get(link)
##    save_cookies(driver, "cfduid")
# driver.quit()
##    cookies = pickle.load(open("cfduid", "rb"))
# for i in cookies:
# if i['name'] == '__cfduid':
##            cfduid = i['value']
    # end
    num1 = r.text.find('<h1>')
    num2 = r.text.find('<', num1 + 1)
    #aniname = r.text[num1+4:num2]
    print("Name: ", aniname)
    if ':' in aniname:
        aniname = aniname.replace(':', ' -')
    succ = 0
    print(anime_id)

    while(succ == 0):
        try:
            r = requests.get("https://animepahe-com.translate.goog/api?m=release&id=" +
                             anime_id+"&l=30&sort=episode_asc&page=1")
        except:
            pass
        else:
            succ += 1

    jsonResponse = r.json()
    epno = jsonResponse['total']
    print("Total episodes: ", epno)
    try:
        if os.stat(os.path.expanduser('~') + "/.config/senpahe_lastep.json").st_size == 0:
            with open(os.path.expanduser('~') + '/.config/senpahe_lastep.json', 'w') as f:
                ddd = {"dummy": {"ep": 1, "res": 1}}
                json.dump(ddd, f, ensure_ascii=False, indent=4)
    except FileNotFoundError:
        with open(os.path.expanduser('~') + '/.config/senpahe_lastep.json', 'w') as f:
            ddd = {"dummy": {"ep": 1, "res": 1}}
            json.dump(ddd, f, ensure_ascii=False, indent=4)

    cntu = open(os.path.expanduser('~') + "/.config/senpahe_lastep.json", "rb")
    temp = json.load(cntu)
    present = aniname in temp
    if present is True:
        print()
        choice = input(
            "\nFound previous watch data! Resume from ep "+str(temp[aniname]['ep'])+"?(y/n)")

        if(choice == 'y'):
            start = temp[aniname]["ep"]
            res = temp[aniname]["res"]
        else:
            start = int(input("Enter episode to start at: "))
            res = 0
    else:

        start = int(input("Enter episode to start at: "))
        res = 0
    cntu.close()
    end = int(epno)


##    cdn = input("\nWanna use CDN?(y/n)(not available on saturdays and sundays, check during the first stage to be sure) ")
    cdn = 'n'
    x = []

    direct = dict()
    lcc = ''
    for i in range(start, end + 1):
        current = i
        multi = int(i/30)
        if i % 30 == 0:
            multi -= 1
        index = int(i - 1 - (multi*30))
        succ = 0
        while(succ == 0):
            try:
                r = requests.get("https://animepahe-com.translate.goog/api?m=release&id=" +
                                 anime_id+"&l=30&sort=episode_asc&page=" + str(multi + 1))
            except:
                pass
            else:
                succ += 1

        jsonResponse = r.json()
        session = jsonResponse['data'][index]['session']
        # print(session)
        succ = 0
        while(succ == 0):
            try:
                rr = requests.get(
                    "https://animepahe-com.translate.goog/api?m=links&id="+anime_id+"&session="+session)
            except:
                pass
            else:
                succ += 1

        jsonResponse = rr.json()
        reslist = jsonResponse['data']

        if i == start:
            print("\nAvailable resolutions for episode(s) are:" + '\n')
            if start - end == 0:

                for j in reslist:
                    tmp = ''
                    tmp1 = float(j[str(list(j))[2:-2]]['filesize'])
                    byt = ''
                    if tmp1 > 1073741824:
                        byt = str(
                            float("{:.2f}".format(tmp1/1073741824))) + ' GB'
                    else:
                        byt = str(float("{:.2f}".format(tmp1/1048576))) + ' MB'
                    if j[str(list(j))[2:-2]]['hq'] == 1:
                        tmp = str(list(j))[2:-2] + 'p' + ' Fansub Provider: ' + str(
                            j[str(list(j))[2:-2]]['fansub']) + ' Size: ' + byt + ' HQ(Larger size)'
                    else:
                        tmp = str(list(j))[2:-2] + 'p' + ' Fansub Provider: ' + \
                            str(j[str(list(j))[2:-2]]['fansub']) + \
                            ' Size: ' + byt

                    x.append(str(list(j)))
                    print(tmp)
            else:

                for j in reslist:
                    tmp = ''
                    if j[str(list(j))[2:-2]]['hq'] == 1:
                        tmp = str(list(j))[
                            2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])
                    else:
                        tmp = str(list(j))[
                            2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])

                    x.append(str(list(j)))
                    print(tmp)
            if res == 0:
                res = int(input(
                    "\nWhich resolution do you want to stream in?(1 for first one, 2 for second one) "))
            else:
                input(
                    "\nWhich resolution do you want to stream in?(1 for first one, 2 for second one) (" + str(res) + ")")

        try:
            b = jsonResponse['data'][res - 1][x[res-1][2:-2]]['kwik']
        except:
            os.system(
                'notify-send "senpahe" "You need to choose the resolution again"')
            print("\nAvailable resolutions for episode(s) are:" + '\n')
            y = []
            for j in reslist:
                tmp = ''
                if j[str(list(j))[2:-2]]['hq'] == 1:
                    tmp = str(list(j))[
                        2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])
                else:
                    tmp = str(list(j))[
                        2:-2] + 'p' + ' Fansub Provider: ' + str(j[str(list(j))[2:-2]]['fansub'])

                y.append(str(list(j)))
                print(tmp)
            res = int(input(
                "\nWhich resolution do you want to download in?(1 for first one, 2 for second one) "))
            b = jsonResponse['data'][res - 1][y[res-1][2:-2]]['kwik']
            siz = jsonResponse['data'][res - 1][y[res-1][2:-2]]['filesize']
        else:
            siz = jsonResponse['data'][res - 1][x[res-1][2:-2]]['filesize']

        referer = "https://animepahe.com/play/" + anime_session + "/" + session
        # get link

# def dlpage(kw, ref):
##            retries = Retry(total=100, backoff_factor=1, status_forcelist=[429, 500, 502, 503, 504])
##            http = requests.Session()
##            http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
# try:
##                rf = http.get(kw, headers={'referer':ref}).content
# except:
##                rf = download(kw, ref)
# return rf
##        rr = dlpage(b, referer)
# print(rr)
        getnamelink = 'curl "' + b + '" -H "Referer: ' + referer + '"'

        print(getnamelink)
        rr = os.popen(getnamelink).read()
        epname = rr[rr.find("Anime"):rr.find(".mp4") + 4]

        try:
            os.chdir(savfinal)
        except:
            pass
        rt = rr.find("/snapshot/")
        uwu = ''
        ll = rr[rt+10:rr.find('.', rt+10)]
        tmp = ''
        if lcc == '':
            uwu = svrscrape(ll, b)
            os.remove('done.aa')
            with open("uwu.m3u8", 'r') as f:
                s = ''
                while s[0:5] != 'https':
                    s = f.readline()
                lcc = s[11:s.find('.', 11)]
        else:
            def dluwu(uwu, ref):
                retries = Retry(total=100, backoff_factor=1,
                                status_forcelist=[429, 500, 502, 503, 504])
                http = requests.Session()
                http.mount("https://", TimeoutHTTPAdapter(max_retries=retries))
                try:
                    r = http.get(uwu, headers={'referer': ref}, timeout=5)
                    print("status_code is ", r.status_code)
                    if r.status_code == 200:
                        pass
                    else:
                        return -1
                except:
                    f.close()
                    download(uwu, ref)
                try:
                    f.close()
                except:
                    pass
                return 0
            uwu = "https://eu-"+lcc+".files.nextcdn.org/stream/" + ll + "/uwu.m3u8"
            if dluwu(uwu, b) == -1:
                print("!!!!!!!SERVER CHANGED!!!!!!!!")
                uwu = svrscrape(ll, b)
                os.remove('done.aa')
                with open("uwu.m3u8", 'r') as f:
                    s = ''
                    while s[0:5] != 'https':
                        s = f.readline()
                    lcc = s[11:s.find('.', 11)]

        print(b)
        print(uwu)

        print(ll)

        print(i)

        if current == start:
            tas = threading.Thread(target=mpvplay, args=(uwu, b,))
            yas = threading.Thread(target=lastep, args=(aniname,res,start,))
            tas.start()
            time.sleep(5)
            yas.start()
        else:
            #print("echo loadfile '" + uwu + "' append-play | socat - /tmp/mpvsocket")
            os.system("echo loadfile '" + uwu + "' append-play | socat - /tmp/mpvsocket")
        if i == end:
            tas.join()
            yas.join()
            print("Finished!")
            sys.exit(0)
        else:
            continue

    #download(direct, savfinal)
    print('\n' + "Queue Finished!")
    #input("Press enter key to exit...")
    #sys.exit(0)

# try:
#    main()
# except KeyboardInterrupt:
#    print("Interrupted by user")
